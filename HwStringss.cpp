﻿
#include <iostream>
#include <string>

int main()
{
    /*Создаем и инициализируем переменную одной строкой,
    потому что так принято*/
    std::string resolution = "I am going to make great games!\n";
    std::cout << resolution;
    /*Выводя на экран данные о длинне строки не забываем,
    что стоит отбить после нее новую строку*/
    std::cout << resolution.length() << "\n";
    /*Находим и печатаем первый и последний символы 
    с помощью оператора [].
    число 30 использовано потому, что иначе оператор находит 
    команду создания новой строки*/
    std::cout << resolution[0] << "\n" << resolution [30];

    return 1;
}
